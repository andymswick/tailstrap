module.exports = {
  theme: {
    extend: {
      colors: {
        'primary': "theme('colors.blue.500')",
        'secondary': "theme('colors.gray.600')",
        'success': "theme('colors.green.500')",
        'info': "theme('colors.indigo.500')",
        'warning': "theme('colors.yellow.500')",
        'danger': "theme('colors.red.500')",
        'light': "theme('colors.gray.100')",
        'dark': "theme('colors.gray.800')",
      },
      borderRadius: {
        'md': '.3rem'
      },
      boxShadow: {
        'active': 'inset 0 3px 5px rgba(#000000, .125)',
        'active-focus': "0 0 0 .2rem rgba(theme('colors.blue.500'), .25), inset 0 3px 5px rgba(#000000, .125)",
        'focus': "0 0 0 .2rem rgba(theme('colors.blue.500'), .25)",
      },
      height: {
        '3/20': '15%',
      },
      inset: {
        '3/20': '15%',
        '20px': '20px',
      },
      padding: {
        '1-5': '.375rem',
        '20px': '20px',
      },
      margin: {
        '-full': '-100%'
      },
      opacity: {
        65: '.65',
        90: '.90'
      },
      textColor: {
        'inherit': 'inherit',
      },
      width: {
        '3/20': '15%',
        'auto': 'auto'
      },
      zIndex: {
        1: 1,
        15: 15
      }
    }
  },
  variants: {},
  plugins: []
}
