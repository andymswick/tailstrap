const mix = require('laravel-mix');
const tailwindcss = require('tailwindcss');

mix.sass('src/scss/tailstrap.scss', 'build/css')
    .sass('src/scss/example.scss', 'build/css')
    .options({
        processCssUrls: false,
        postCss: [tailwindcss('./tailwind.config.js')],
    });