<h1 align="center">Tailstrap</h1>

<p align="center">
    <a href="https://tailwindcss.com/" target="_blank"><img width="200" src="https://tailwindcss.com/img/tailwind.svg"></a><br>
    A Tailwindcss implemented version of Bootstrap.
</p>

------

## Table of contents

- [Quick start](#quick-start)
- [Status](#status)
- [What's included](#whats-included)
- [Bugs and feature requests](#bugs-and-feature-requests)
- [Documentation](#documentation)
- [Contributing](#contributing)
- [Community](#community)
- [Versioning](#versioning)
- [Creators](#creators)
- [Thanks](#thanks)
- [Copyright and license](#copyright-and-license)


## Getting Started


## What's included

Within the download you'll find the following directories and files, logically grouping common assets and providing both compiled and minified variations. You'll see something like this:

```text
tailstrap/
│
├───build/
│   └───css
│       └── tailstrap.css
│
└───src
    └───scss
        ├── bootstrap-grid.scss
        ├── bootstrap-reboot.scss
        ├── tailstrap.scss
        ├── _alert.scss
        ├── _badge.scss
        ├── _breadcrumb.scss
        ├── _button-group.scss
        ├── _buttons.scss
        ├── _card.scss
        ├── _carousel.scss
        ├── _close.scss
        ├── _code.scss
        ├── _custom-forms.scss
        ├── _dropdown.scss
        ├── _forms.scss
        ├── _functions.scss
        ├── _grid.scss
        ├── _helpers.scss
        ├── _images.scss
        ├── _input-group.scss
        ├── _list-group.scss
        ├── _mixins.scss
        ├── _modal.scss
        ├── _nav.scss
        ├── _navbar.scss
        ├── _pagination.scss
        ├── _popover.scss
        ├── _progress.scss
        ├── _reboot.scss
        ├── _root.scss
        ├── _spinners.scss
        ├── _tables.scss
        ├── _toasts.scss
        ├── _tooltip.scss
        ├── _transitions.scss
        ├── _type.scss
        ├── _utilities.scss
        ├── _variables.scss
        │
        ├───helpers
        │   ├── _background.scss
        │   ├── _clearfix.scss
        │   ├── _embed.scss
        │   ├── _position.scss
        │   ├── _screenreaders.scss
        │   ├── _stretched-link.scss
        │   └── _text.scss
        │
        ├───mixins
        │   ├── _alert.scss
        │   ├── _background-variant.scss
        │   ├── _border-radius.scss
        │   ├── _box-shadow.scss
        │   ├── _breakpoints.scss
        │   ├── _buttons.scss
        │   ├── _caret.scss
        │   ├── _clearfix.scss
        │   ├── _deprecate.scss
        │   ├── _forms.scss
        │   ├── _gradients.scss
        │   ├── _grid-framework.scss
        │   ├── _grid.scss
        │   ├── _image.scss
        │   ├── _list-group.scss
        │   ├── _lists.scss
        │   ├── _nav-divider.scss
        │   ├── _pagination.scss
        │   ├── _reset-text.scss
        │   ├── _resize.scss
        │   ├── _screen-reader.scss
        │   ├── _table-row.scss
        │   ├── _text-emphasis.scss
        │   ├── _text-truncate.scss
        │   ├── _transition.scss
        │   └── _utilities.scss
        │
        ├───utilities
        │   ├── _api.scss
        │   ├── _sizing.scss
        │   ├── _text.scss
        │   └── _visibility.scss
        │
        └───vendor
            └── _rfs.scss
```

We provide compiled CSS (`tailstrap.*`), as well as compiled and minified CSS (`tailstrap.min.*`).


